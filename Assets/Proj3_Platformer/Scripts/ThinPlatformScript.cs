using UnityEngine;
using System.Collections;

//alters layers so that any player coming in contact will not collide with platforms
//

public class ThinPlatformScript : MonoBehaviour 
{
	void OnTriggerEnter(Collider other) 
	{
        other.gameObject.layer = LayerMask.NameToLayer("Passthrough");
    }
	
	void OnTriggerExit(Collider other)
	{
		other.gameObject.layer = LayerMask.NameToLayer("Character");
	}
}
