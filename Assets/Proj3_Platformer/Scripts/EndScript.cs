using UnityEngine;
using System.Collections;

public class EndScript : MonoBehaviour 
{
	public GameObject angelText;
	
	void Start()
	{
		int angels = PlayerPrefs.GetInt("Angels");
		
		if(angels == 0)
		{
			angelText.SetActive(true);
		}
		else
		{
			angelText.SetActive(false);
		}
	}
	
	void Update () 
	{
		if(Input.anyKey)
		{
			Application.LoadLevel("StartScene");
		}
	}
}
