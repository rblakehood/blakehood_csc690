using UnityEngine;
using System.Collections;

//interface for the player to control their character

[RequireComponent(typeof(Motor))]
public class PlayerController : MonoBehaviour 
{
	public float speed = 10;
	
	Motor motor;
	
	void Start () 
	{
		motor = GetComponent<Motor>();
		motor.speed = speed;
	}
	
	void Update () 
	{
		motor.targetVelocity.x = Input.GetAxis("Horizontal") * speed;
		motor.shouldJump = Input.GetButtonDown("Jump");
	}
	
	void OnCollisionEnter(Collision collision)
	{
		if(collision.collider.gameObject.layer == LayerMask.NameToLayer("Character") || collision.collider.gameObject.layer == LayerMask.NameToLayer("Passthrough"))
			Destroy(this.gameObject);
	}
	
	public delegate void Died();
	public static event Died died = delegate {};
	
	void OnDestroy()
	{
		died();
	}
}
