using UnityEngine;
using System.Collections;

public class HellPitDestroyScript : MonoBehaviour 
{
	void OnTriggerEnter(Collider other)
	{
		if(other.tag != "Player")
			Destroy(other.gameObject);
		else
		{
			if(GameObject.FindGameObjectsWithTag("runner").Length == 0)
			{
				PlayerPrefs.SetInt("Angels", GameObject.FindGameObjectsWithTag("chaser").Length);
				
				Application.LoadLevel("EndScene");
			}
			else
			{
				other.rigidbody.velocity = new Vector3(other.rigidbody.velocity.x, 10, other.rigidbody.velocity.z);
			}
		}
	}
}
