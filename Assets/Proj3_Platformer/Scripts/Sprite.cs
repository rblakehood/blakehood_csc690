using UnityEngine;
using System;
using System.Collections;
using System.Runtime.CompilerServices;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
[assembly:InternalsVisibleTo("AnimationHandler")]
public class Sprite : MonoBehaviour
{
	//vertex colors
	//undo and redo stack
	
	public SpriteSheet sheet;
	
	[SerializeField]
	[HideInInspector]
	string _mainName;
	public string mainName
	{
		get { return _mainName; }
		set
		{
			_mainName = value;
		
			walkFrames = 0;
			foreach(SpriteInfo si in sheet.spriteInfos)
			{
				if(si.name.StartsWith(value + "walk"))
				{
					walkFrames++;
				}
			}
		}
	}
	
	[SerializeField]
	[HideInInspector]
	int facing = 0; //0 is standing, 1 is right, -1 is left
	[SerializeField]
	[HideInInspector]
	int _walkSequence = 0;
	[SerializeField]
	[HideInInspector]
	int walkFrames = 4;
	[SerializeField]
	[HideInInspector]
	int walkSequence
	{
		get {return _walkSequence;}
		set
		{
			if(value >= walkFrames)
			{
				_walkSequence = 0;
			}
			else
			{
				_walkSequence = value;
			}
		}
	}
	
	void Start()
	{
		createMesh();
	}
	
	[ContextMenu("Create Mesh")]
	public void createMesh()
	{
		
		if(mainName == null || sheet == null)
			return;
		
		String spriteName = mainName;
		
		//hack to easily allow this one class to be used for all sprites in this game
		try
		{
			if(this.GetComponent<Motor>().isOnWall())
			{
				spriteName = mainName + "wall";
			}
			else if(this.GetComponent<Motor>().isInAir())
			{
				spriteName = mainName + "jump";
			}
			else if(facing != 0)
			{
				spriteName = mainName + "walk" + walkSequence.ToString();
				walkSequence++;
			}
		}
		catch(Exception e)
		{
			if(facing != 0)
			{
				spriteName = mainName + "walk" + walkSequence.ToString();
				walkSequence++;
			}
		}
		
		SpriteInfo info = sheet.getSpriteInfo(spriteName);
		
		if(info == null)
			return;

		Vector3[] verts = new Vector3[4];
		verts[0] = new Vector3(-0.5f * collider.bounds.size.x/transform.localScale.x, -0.5f * collider.bounds.size.y/transform.localScale.y, 0);
		verts[1] = new Vector3(0.5f * collider.bounds.size.x/transform.localScale.x, -0.5f * collider.bounds.size.y/transform.localScale.y, 0);
		verts[2] = new Vector3(0.5f * collider.bounds.size.x/transform.localScale.x, 0.5f * collider.bounds.size.y/transform.localScale.y, 0);
		verts[3] = new Vector3(-0.5f * collider.bounds.size.x/transform.localScale.x, 0.5f * collider.bounds.size.y/transform.localScale.y, 0);
		
		Vector2[] uvs = new Vector2[4];
		uvs[0] = new Vector2(info.uvs.x, info.uvs.y);
		uvs[1] = new Vector2(info.uvs.x + info.uvs.width, info.uvs.y);
		uvs[2] = new Vector2(info.uvs.x + info.uvs.width, info.uvs.y + info.uvs.height);
		uvs[3] = new Vector2(info.uvs.x, info.uvs.y + info.uvs.height);
		
		if(facing == -1)
		{
			Vector2 temp = uvs[0];
			uvs[0] = uvs[1];
			uvs[1] = temp;
			
			temp = uvs[2];
			uvs[2] = uvs[3];
			uvs[3] = temp;
		}
		
		int[] tris = {3,1,0,3,2,1};
		
		Vector3[] norms = {Vector3.forward, Vector3.forward, Vector3.forward, Vector3.forward};
		
		Mesh mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
		
        mesh.vertices = verts;
		mesh.normals = norms;
        mesh.uv = uvs;
        mesh.triangles = tris;

		renderer.material = sheet.material;
	}
	
	public void setFacing(int f)
	{
		if(f >= 1)
		{
			facing = 1;
		}
		else if (f <= -1)
		{
			facing = -1;
		}
		else
		{
			facing = 0;
		}
	}
	
	public int getFacing()
	{
		return facing;
	}
}
