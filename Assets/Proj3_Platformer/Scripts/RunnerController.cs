using UnityEngine;
using System.Collections;

//an object containing this script will run from the player
//starts and stops running at set distances from player

[RequireComponent(typeof(Motor))]
public class RunnerController : NPCController
{
	protected override void handleMovement()
	{
		int direction = (this.transform.position.x < player.transform.position.x) ? -1 : 1;
		motor.targetVelocity.x = speed * direction;

		if(motor.isOnGround())
		{
			groundY = this.transform.position.y;
			motor.shouldJump = this.transform.position.y >= player.transform.position.y - jumpThreshold;
		}
		else if(this.transform.position.y > groundY + motor.jumpHeight - airJumpBuffer || this.transform.position.y < groundY - airJumpBuffer)
		{
			motor.shouldJump = this.transform.position.y >= player.transform.position.y - airJumpBuffer;
		}
	}
	
	public delegate void Died();
	public static event Died died = delegate {};
	
	void OnDestroy()
	{
		died();
	}
}
