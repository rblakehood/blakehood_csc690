using UnityEngine;
using System.Collections;

public class HellPitDisableScript : MonoBehaviour 
{
	void OnTriggerEnter(Collider other) 
	{
		if(other.tag != "Player" && other.gameObject.layer == LayerMask.NameToLayer("Character"))
			other.GetComponent<Motor>().stop();
	}
}
