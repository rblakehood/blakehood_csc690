using UnityEngine;
using System.Collections;

public class RespawnScript : MonoBehaviour 
{
	public static int lives = 3;
	
	private static int livesLeft;
	
	public Transform playerPrefab;
	
	void Start () 
	{
		if(livesLeft <= 0)
		{
			livesLeft = lives;
		}
		
		PlayerController.died += reduceLives;
	}
	
	void Update () 
	{
		if(!GameObject.FindGameObjectWithTag("Player"))
		{
			if(lives == 0)
			{
				Application.LoadLevel("StartScene");
			}
			else if(Input.GetButtonDown("Jump"))
			{
				GameObject.Instantiate(playerPrefab, this.transform.position, Quaternion.identity);
			}
		}
	}
	
	void reduceLives()
	{
		lives--;
	}
}
