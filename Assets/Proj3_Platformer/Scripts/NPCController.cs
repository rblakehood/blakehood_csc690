using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Motor))]
public abstract class NPCController : MonoBehaviour 
{
	public float speed = 10;
	public float jumpThreshold = 1;
	public float airJumpBuffer = 0.5f;
	public float lockDistance = 10;
	public float unlockDistance = 15;
	
	protected static GameObject player = null;
	
	protected Motor motor;
	protected float groundY = 0;
	protected bool locked = false;
	
	void Start () 
	{
		if(!player)
		{
			player = GameObject.FindGameObjectWithTag("Player");
		}
		
		motor = GetComponent<Motor>();
		motor.speed = speed;
		groundY = this.transform.position.y;
	}
	
	
	void Update () 
	{
		if(!player)
		{
			player = GameObject.FindGameObjectWithTag("Player");
			return;
		}
		
		if(locked)
		{
			checkForUnlock();
		}
		else
		{
			checkForPlayer();
		}
		
		if(locked)
		{
			handleMovement();
		}
		else
		{
			stopMovement();
		}
	}
	
	void checkForPlayer()
	{
		float dist = playerDistanceSquared();
		float limit = lockDistance * lockDistance;
		
		if(dist <= limit)
		{
			locked = true;
		}
	}
	
	void checkForUnlock()
	{
		float dist = playerDistanceSquared();
		float limit = unlockDistance * unlockDistance;
		
		if(dist >= limit)
		{
			locked = false;
		}
	}
	
	float playerDistanceSquared()
	{
		return Mathf.Pow(this.transform.position.x - player.transform.position.x, 2) + Mathf.Pow(this.transform.position.y - player.transform.position.y, 2);
	}
	
	void stopMovement()
	{
		motor.targetVelocity.x = 0;
	}
	
	protected abstract void handleMovement();
}
