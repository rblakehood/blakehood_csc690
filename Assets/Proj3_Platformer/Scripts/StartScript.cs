using UnityEngine;
using System.Collections;

//start screen

public class StartScript : MonoBehaviour 
{
	void Start()
	{
		RespawnScript.lives = 3;
		PlayerPrefs.SetInt("AngelsDestroyed", 1);
	}
	
	void Update () 
	{
		if(Input.anyKey)
		{
			Application.LoadLevel("PlayScene");
		}
	}
}
