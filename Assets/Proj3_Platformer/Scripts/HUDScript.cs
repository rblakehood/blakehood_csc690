using UnityEngine;
using System.Collections.Generic;

public class HUDScript : MonoBehaviour 
{
	int numPlayers = 0; //actually lives, but I like the name consistency sicne it's all HUD sprites
	int numChasers = 0;
	int numRunners = 0;
	
	public Transform playerSprite;
	public Transform chaserSprite;
	public Transform runnerSprite;
	
	List<Transform> playerIcons;
	List<Transform> chaserIcons;
	List<Transform> runnerIcons;
	
	void Start () 
	{
		numPlayers = RespawnScript.lives;
		numChasers = GameObject.FindGameObjectsWithTag("chaser").Length;
		numRunners = GameObject.FindGameObjectsWithTag("runner").Length;
		
		playerIcons = new List<Transform>();
		chaserIcons = new List<Transform>();
		runnerIcons = new List<Transform>();
		
		PlayerController.died += removePlayer;
		ChaserController.died += removeChaser;
		RunnerController.died += removeRunner;
		
		drawHUD();
	}
	
	void drawHUD()
	{
		for(int i = 0; i < numPlayers; i++)
		{
			Vector3 thisPos = this.transform.position;
			Vector3 spritePos = new Vector3(thisPos.x - 12.8f + i, thisPos.y + 9, 0);
			Transform go = (Instantiate(playerSprite, spritePos, Quaternion.identity) as Transform);
			playerIcons.Add(go);
		}
		
		for(int i = 0; i < numChasers; i++)
		{
			Vector3 thisPos = this.transform.position;
			Vector3 spritePos = new Vector3(thisPos.x + 12.8f - i, thisPos.y + 9, 0);
			Transform go = (Instantiate(chaserSprite, spritePos, Quaternion.identity) as Transform);
			chaserIcons.Add(go);
		}
		
		for(int i = 0; i < numRunners; i++)
		{
			Vector3 thisPos = this.transform.position;
			Vector3 spritePos = new Vector3(thisPos.x + 12.8f - i, thisPos.y + 7, 0);
			Transform go = (Instantiate(runnerSprite, spritePos, Quaternion.identity) as Transform);
			runnerIcons.Add(go);
		}
	}
	
	void removeLast(List<Transform> icons)
	{
		GameObject.Destroy(icons[icons.Count-1].gameObject);
		icons.RemoveAt(icons.Count-1);
	}
	
	void removePlayer()
	{
		removeLast(playerIcons);
	}
	
	void removeChaser()
	{
		removeLast(chaserIcons);
	}
	
	void removeRunner()
	{
		removeLast(runnerIcons);
	}
}
