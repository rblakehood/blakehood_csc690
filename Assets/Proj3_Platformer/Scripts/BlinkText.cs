using UnityEngine;
using System.Collections;

//causes the component TextMesh to blink in and out of visibility for a specified amount of time

[RequireComponent(typeof(TextMesh))]
public class BlinkText : MonoBehaviour 
{
	private TextMesh textMesh;
	private string text;
	private int count = 0;
	public float secondsVisible = 1f;
	public float secondsInvisible = 0.5f;
	public float interval = 0.5f;

	// Use this for initialization
	void Start () 
	{
		InvokeRepeating("Blink", 0, interval);
		textMesh = (TextMesh) this.GetComponent(typeof(TextMesh));
		text = textMesh.text.ToString();
	}
	
	void Blink()
	{
		if(textMesh.text != "")
		{
			if(count < (secondsVisible/interval))
			{
				count++;
			}
			else
			{
				count = 0;
				textMesh.text = "";
			}
		}
		else
		{
			if(count < (secondsInvisible/interval))
			{
				count++;
			}
			else
			{
				count = 0;
				textMesh.text = text;
			}
		}
	}
}
