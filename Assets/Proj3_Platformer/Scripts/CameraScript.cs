using UnityEngine;
using System.Collections;

//camera follows the player

public class CameraScript : MonoBehaviour 
{
	GameObject player;
	
	void Start () 
	{
		player = GameObject.FindGameObjectWithTag("Player");
	}
	
	void Update () 
	{
		if(player)
		{
			this.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, this.transform.position.z);
		}
		else
		{
			player = GameObject.FindGameObjectWithTag("Player");
		}
	}
}
