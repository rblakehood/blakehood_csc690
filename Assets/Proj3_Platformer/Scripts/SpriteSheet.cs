using UnityEngine;
using System;
using System.IO;

[Serializable]
public class SpriteInfo
{
	public string name;
	public Rect frame;
	public Rect uvs;
}

[Serializable]
public class SpriteSheet : ScriptableObject
{
	public Texture2D sheetTexture;
	public Material material;
	public Texture2D[] sourceTextures;
	
	[SerializeField]
	public SpriteInfo[] spriteInfos;
	
	[SerializeField]
	private int _maxSheetSize = 1024;
	public int maxSheetSize
	{
		get { return _maxSheetSize; }
		set
		{
			int i = roundToNearestPowerOfTwo(value);
			
			if( i > 1024)
			{
				i = 1024;
			}
			else if(i < 1)
			{
				i = 1;
			}
			
			_maxSheetSize = i;
		}
	}
	
	public Vector2 sheetSize
	{
		get
		{
			if(!sheetTexture)
			{
				return Vector2.zero;
			}
			
			return new Vector2(sheetTexture.width, sheetTexture.height);
		}
	}
	
	
	public SpriteInfo getSpriteInfo(string n)
	{
		foreach(SpriteInfo si in spriteInfos)
		{
			if(String.Equals(si.name, n))
				return si;
		}
		
		return null;
	}
	
	int roundToNearestPowerOfTwo(int n)
	{
		n--;
		n |= n >> 1;
		n |= n >> 2;
		n |= n >> 4;
		n |= n >> 8;
		n |= n >> 16;
		n++;
		
		return n;
	}
}
