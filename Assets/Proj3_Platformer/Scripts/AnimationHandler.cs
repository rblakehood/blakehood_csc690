using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Sprite))]
public class AnimationHandler : MonoBehaviour 
{
	public bool constantAnimation = false;
	public int framesBetweenUpdates = 5; //hack to slow down animation
	
	int framesUntilNextUpdate = 0;
	
	void Update () 
	{
		if(framesUntilNextUpdate > 0)
		{
			framesUntilNextUpdate--;
			return;
		}
		
		Sprite sprite = GetComponent<Sprite>();
		
		if(constantAnimation)
		{
			sprite.setFacing(1);
		}
		else if(this.rigidbody.velocity.x > 0)
		{
			sprite.setFacing(1);
			
		}
		else if(this.rigidbody.velocity.x < 0)
		{
			sprite.setFacing(-1);
		}
		else if(this.rigidbody.velocity.y <= 0)
		{
			sprite.setFacing(0);
		}
		
		sprite.createMesh();
		
		framesUntilNextUpdate = framesBetweenUpdates;
	}
}
