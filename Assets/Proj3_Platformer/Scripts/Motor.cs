using UnityEngine;
using System.Collections;

//handles character movement

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class Motor : MonoBehaviour 
{
	public float maxSpeedChange = 1;
	public float jumpHeight = 5;
	public int airJumps = 1;
	
	bool grounded = false;
	bool wallJump = false;
	Vector3 contactNorm = Vector3.zero;
	int airJumpsRemaining = 0;
	bool stopped = false;

	[HideInInspector]
	public bool shouldJump = false;
	[HideInInspector]
	public Vector3 targetVelocity = Vector3.zero;
	[HideInInspector]
	public float speed = 1; // should only be changed through controller
	
	void FixedUpdate () 
	{
		if(stopped)
			return;
		
		Vector3 velocity = rigidbody.velocity;
		Vector3 velocityChange = (targetVelocity - velocity);
		velocityChange = velocityChange.normalized * Mathf.Clamp(velocityChange.magnitude, 0, maxSpeedChange);
		velocityChange.y = 0;
		
		if(shouldJump && (grounded || airJumpsRemaining > 0))
		{
			shouldJump = false;
			if(!grounded)
			{
				airJumpsRemaining--;
			}
			
			grounded = false;
			
			Vector3 jumpDirection = new Vector3(velocity.x, calculateJumpVerticalSpeed(), velocity.z);
			if(wallJump)
			{
				wallJump = false;
				jumpDirection.x = contactNorm.x * speed;
			}
			rigidbody.velocity = jumpDirection;
		}
		
		rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);
	}
	
	float calculateJumpVerticalSpeed()
	{
		float g = Physics.gravity.y;
		
		return Mathf.Sqrt(2 * jumpHeight * -g);
	}
	
	void OnCollisionEnter(Collision collision)
	{
		shouldJump = false;
		foreach(ContactPoint cp in collision.contacts)
		{
			contactNorm = cp.normal;
			Debug.DrawRay(cp.point, cp.normal, Color.white);
			
			if(Mathf.Abs(contactNorm.y) < Mathf.Abs(contactNorm.x)) //against wall
			{
				grounded = true;
				airJumpsRemaining = airJumps;
				wallJump = true;
			}
			else if(contactNorm.y > Mathf.Abs(contactNorm.x)) //on floor
			{
				grounded = true;
				airJumpsRemaining = airJumps;
			}
		}
	}
	
	void OnCollisionExit(Collision collision)
	{
		wallJump = false;
		shouldJump = false;
		
		foreach(ContactPoint cp in collision.contacts)
		{
			contactNorm = cp.normal;
			
			if(contactNorm.y > Mathf.Abs(contactNorm.x)) //on floor
			{
				grounded = false;
			}
		}
	}
	
	public bool isOnGround()
	{
		return grounded;
	}
	
	public bool isInAir()
	{
		return !grounded;
	}
	
	public int getAirJumpsRemaining()
	{
		return airJumpsRemaining;
	}
	
	public bool isOnWall()
	{
		return wallJump;
	}
	
	public void stop()
	{
		stopped = true;
		targetVelocity = Vector3.zero;
		shouldJump = false;
	}
	
	public void unStop()
	{
		stopped = false;
	}
}
