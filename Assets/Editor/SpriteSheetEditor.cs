using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.IO;

[CustomEditor(typeof(SpriteSheet))]
[CanEditMultipleObjects]
public class SpriteSheetEditor : Editor 
{
	public SpriteSheet[] sheets
	{
		get
		{
			SpriteSheet[] sheets = targets as SpriteSheet[];
			
			if(sheets == null)
			{
				sheets = new SpriteSheet[1] {target as SpriteSheet};
			}
			
			return sheets;
		}
	}
	
	public string[] sheetNames;
	
	[MenuItem("Assets/Create/SpriteSheet")]
	public static void CreateAsset()
	{
		SpriteSheet sheet = ScriptableObject.CreateInstance<SpriteSheet>();
		
		string path = AssetDatabase.GetAssetPath(Selection.activeObject);
		if(path == "")
		{
			path = "Assets";
		}
		else if(Path.GetExtension(path) != "")
		{
			path = Path.GetDirectoryName(path);
		}
		path = AssetDatabase.GenerateUniqueAssetPath(path + "/NewSpriteSheet.asset");
		
		AssetDatabase.CreateAsset(sheet, path);
		AssetDatabase.SaveAssets();
		EditorUtility.FocusProjectWindow();
		Selection.activeObject = sheet;
	}
	
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		
		if(GUILayout.Button("Create Spritesheet"))
		{
			createSpriteSheet();
		}
	}
	
	void createSpriteSheet()
	{
		int sheetNum = 0;
		foreach(SpriteSheet sheet in sheets)
		{
			foreach(Texture2D texture in sheet.sourceTextures)
			{
				TextureImporter importer = AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(texture.GetInstanceID())) as TextureImporter;
				importer.isReadable = true;
				importer.npotScale = TextureImporterNPOTScale.None;
				importer.textureFormat = TextureImporterFormat.AutomaticTruecolor;
				AssetDatabase.ImportAsset(importer.assetPath);
			}
		
			sheet.sheetTexture = new Texture2D(sheet.maxSheetSize, sheet.maxSheetSize);
			Rect[] uvRects = sheet.sheetTexture.PackTextures(sheet.sourceTextures, 0, sheet.maxSheetSize);
		
			sheet.spriteInfos = new SpriteInfo[sheet.sourceTextures.Length];
		
			for(int i = 0; i < sheet.sourceTextures.Length; i++)
			{
				SpriteInfo info = new SpriteInfo();
				info.name = sheet.sourceTextures[i].name;
				info.frame = new Rect(0, 0, sheet.sourceTextures[i].width, sheet.sourceTextures[i].height);
				info.uvs = uvRects[i];
			
				sheet.spriteInfos[i] = info;
			}
		
			byte[] byteArray = sheet.sheetTexture.EncodeToPNG();
			string path = Path.GetDirectoryName(AssetDatabase.GetAssetPath(sheet.GetInstanceID()));
			string texturePath = AssetDatabase.GenerateUniqueAssetPath(path + "/sheet" + sheetNum + ".png");
			File.WriteAllBytes(texturePath, byteArray);
			AssetDatabase.ImportAsset(texturePath);
			sheet.sheetTexture = AssetDatabase.LoadMainAssetAtPath(texturePath) as Texture2D;
			
			Material mat = new Material(Shader.Find("Mobile/Particles/Alpha Blended"));
			mat.mainTexture = sheet.sheetTexture;
			string matPath = AssetDatabase.GenerateUniqueAssetPath(path + "/sheet" + sheetNum + ".mat");
			AssetDatabase.CreateAsset(mat, matPath);
			sheet.material = AssetDatabase.LoadMainAssetAtPath(matPath) as Material;
			AssetDatabase.SaveAssets();
			
			sheetNum++;
		}
	}
}
