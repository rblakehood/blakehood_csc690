using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(Sprite))]
public class SpriteEditor : Editor 
{
	List<string> optionsList;
	string[] options;
	int index = 0;
	
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Sprite Name");
		
		Sprite sprite = target as Sprite;
		getOptions(sprite);
		index = EditorGUILayout.Popup(index, options);
		sprite.mainName = options[index];
		EditorGUILayout.EndHorizontal();
	}
	
	//only want primary sprite names, not every form of every sprite
	void getOptions(Sprite sprite)
	{
		optionsList = new List<string>();
		
		foreach(SpriteInfo si in sprite.sheet.spriteInfos)
		{
			if(si.name.EndsWith("jump"))
				continue;
			if(si.name.EndsWith("wall"))
				continue;
			if(si.name.Contains("walk"))
				continue;
			
			optionsList.Add(si.name);
		}
		
		options = optionsList.ToArray();
		
		index = optionsList.IndexOf(sprite.mainName);
	}
}
