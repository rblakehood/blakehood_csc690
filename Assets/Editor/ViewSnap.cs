using UnityEngine;
using UnityEditor;
using System.Collections;

public class ViewSnap
{
	[MenuItem("View/Front View %1")]
	static void FrontView()
	{
		getSceneView().orthographic = true;
		getSceneView().LookAt(getSceneView().pivot, Quaternion.LookRotation(Vector3.back));
	}
	
	[MenuItem("View/Back View %2")]
	static void BackView()
	{
		getSceneView().orthographic = true;
		getSceneView().LookAt(getSceneView().pivot, Quaternion.LookRotation(Vector3.forward));
	}
	
	[MenuItem("View/Right View %3")]
	static void RightView()
	{
		getSceneView().orthographic = true;
		getSceneView().LookAt(getSceneView().pivot, Quaternion.LookRotation(Vector3.left));
	}
	
	[MenuItem("View/Left View %4")]
	static void LeftView()
	{
		getSceneView().orthographic = true;
		getSceneView().LookAt(getSceneView().pivot, Quaternion.LookRotation(Vector3.right));
	}
	
	[MenuItem("View/Top View %5")]
	static void TopView()
	{
		getSceneView().orthographic = true;
		getSceneView().LookAt(getSceneView().pivot, Quaternion.LookRotation(Vector3.down));
	}
	
	[MenuItem("View/Bottom View %6")]
	static void BottomView()
	{
		getSceneView().orthographic = true;
		getSceneView().LookAt(getSceneView().pivot, Quaternion.LookRotation(Vector3.up));
	}
	
	[MenuItem("View/Change Projection %7")]
	static void ChangeProjection()
	{
		getSceneView().orthographic = !(getSceneView().orthographic);
	}
	
	static SceneView getSceneView()
	{
		SceneView activeSceneView = null;
		
		if(SceneView.lastActiveSceneView != null)
		{
			activeSceneView = SceneView.lastActiveSceneView;
		}
		else if(SceneView.sceneViews.Count > 0)
		{
			activeSceneView = (SceneView) SceneView.sceneViews[0];
		}
		
		return activeSceneView;
	}
}
