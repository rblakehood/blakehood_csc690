using UnityEngine;
using System.Collections;

public class BallSpawnScript : MonoBehaviour 
{
	public GameObject player;
	public GameObject npc;
	
	public Transform ballPrefab;
	
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			GameObject[] balls = GameObject.FindGameObjectsWithTag("ball");
			
			if(balls.Length <= 1)
			{
				spawnBall(player, Vector3.up);
				
				if(balls.Length == 0)
				{
					spawnBall(npc, Vector3.down);
				}
			}
		}
	}
	
	//yDirection should be Vector3.up or Vector3.down
	//kind of hacky, but super easy to make work
	void spawnBall(GameObject spawner, Vector3 yDirection)
	{
		Vector3 v = new Vector3(spawner.transform.position.x + 2, spawner.transform.position.y + (2 * yDirection.y), spawner.transform.position.z);
		Instantiate(ballPrefab, v, Quaternion.identity);
	}
}
