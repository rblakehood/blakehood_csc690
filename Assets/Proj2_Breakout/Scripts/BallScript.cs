using UnityEngine;
using System.Collections;

public class BallScript : MonoBehaviour 
{
	public float speed = 10;
	public int killPoints = 100;
	private GameObject lastHit = null; //the paddle last touched, for scoring
	private static ScoreScript player;
	private static ScoreScript npc;
	
	void Start () 
	{
		//bounce off the paddle first
		if(this.transform.position.y < 0)
		{
			rigidbody.velocity = Vector3.down * speed;
		}
		else
		{
			rigidbody.velocity = Vector3.up * speed;
		}
		
		player = (ScoreScript) GameObject.Find("Player").GetComponent(typeof(ScoreScript));
		npc = (ScoreScript) GameObject.Find("NPC").GetComponent(typeof(ScoreScript));
	}
	
	void Update () 
	{
		if(transform.position.y < -52)
		{
			npc.addScore(killPoints);
			player.die();
			Destroy(gameObject);
		}
		else if(transform.position.y > 52)
		{
			player.addScore(killPoints);
			npc.die();
			Destroy(gameObject);
		}
		
		float totalVelocity = Vector3.Magnitude(rigidbody.velocity);
		if(totalVelocity > speed)
		{
			rigidbody.velocity /= (totalVelocity / speed);
		}
		else if(totalVelocity < speed)
		{
			rigidbody.velocity /= (totalVelocity / speed);
		}
	}
	
	void OnCollisionEnter(Collision collision)
	{
		if(collision.gameObject.tag.Equals("paddle"))
		{	
			lastHit = collision.gameObject;
			
			float xDist = this.rigidbody.position.x - collision.rigidbody.position.x;
			float length = collision.collider.bounds.size.x;
			
			rigidbody.velocity = new Vector3 ((xDist / length) * speed, rigidbody.velocity.y, rigidbody.velocity.z);
		}
	}
	
	public GameObject getLastHit()
	{
		return lastHit;
	}
}
