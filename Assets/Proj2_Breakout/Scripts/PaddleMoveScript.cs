using UnityEngine;
using System.Collections;

public class PaddleMoveScript : MonoBehaviour 
{
	public float speed = 50;
	
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		rigidbody.velocity = Vector3.right * Input.GetAxis("Horizontal") * speed;
	}
}
