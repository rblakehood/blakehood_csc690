using UnityEngine;
using System.Collections;

public class ScoreScript : MonoBehaviour 
{
	public TextMesh scoreText;
	public TextMesh livesText;
	public HighScoreScript highScoreScript;
	
	private int lives = 3;
	private int score = 0;
	
	void Start () 
	{
		
	}
	
	void Update () 
	{
	}
	
	public void addScore(int s)
	{
		score += s;
		
		highScoreScript.checkScore(score);
		
		scoreText.text = "Score: " + score.ToString();
	}
	
	public void die()
	{
		lives--;
		
		livesText.text = "Lives: " + lives.ToString();
		
		if(lives == 0)
		{
			Application.LoadLevel("Pongout");
		}
	}
}
