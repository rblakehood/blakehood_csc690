using UnityEngine;
using System.Collections;

public class BrickScript : MonoBehaviour 
{
	public int points = 10;
	
	private static int numBricks = 0;
	
	void Start () 
	{	
		//become a random color: red, green, blue, cyan, magenta, yellow, or white
		int red, green, blue;
		do
		{
			red = Random.Range(0, 2);
			green = Random.Range(0, 2);
			blue = Random.Range(0, 2);
		} while ((red + green + blue) == 0);
			
		Color color = new Color(red, green, blue, 1);
		renderer.material.color = color;
		
		numBricks++;
	}
	
	void Update () 
	{
	}
	
	void OnCollisionEnter(Collision collision)
	{
		BallScript ball = (BallScript) collision.gameObject.GetComponent(typeof(BallScript));
		GameObject paddle = ball.getLastHit();
		ScoreScript score = (ScoreScript) paddle.GetComponent(typeof(ScoreScript));
		score.addScore(points);
		
		Destroy(gameObject);
		
		numBricks--;
		
		if(numBricks == 0)
		{
			Application.LoadLevel("Pongout");
		}
	}
}
