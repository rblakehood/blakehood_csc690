using UnityEngine;
using System.Collections;

public class NpcScript : MonoBehaviour 
{
	public float maxDist = 50;
	public float minDiff = 2;
	public float speed = 50;
	public float acceleration = 5;
	
	private GameObject target = null;
	private float steering = 0;
	
	void Start () 
	{
	}
	
	void Update () 
	{
		if(target == null)
		{
			findClosestBall();
		}
		else if(this.transform.position.y - target.transform.position.y > maxDist)
		{
			target = null;
		}
			
		if(target != null)
		{
			float newSteering = 0;
			if(this.transform.position.x - minDiff > target.transform.position.x)
			{
				newSteering = -1;
			}
			else if(this.transform.position.x  + minDiff < target.transform.position.x)
			{
				newSteering = 1;
			}
		
			steering = Mathf.Lerp(steering, newSteering, Time.deltaTime * acceleration);
			rigidbody.velocity = Vector3.right * steering * speed;
		}
		else
		{
			rigidbody.velocity = Vector3.zero;
		}
	}
	
	private void findClosestBall()
	{
		GameObject[] balls = GameObject.FindGameObjectsWithTag("ball");
		float closestDist = Mathf.Infinity;
		
		foreach(GameObject ball in balls)
		{
			float distance = this.transform.position.y - ball.transform.position.y;
			
			if(distance < maxDist && distance < closestDist)
			{
				target = ball;
				closestDist = distance;
			}
		}
	}
}
