using UnityEngine;
using System.Collections;

public class HighScoreScript : MonoBehaviour 
{
	public TextMesh textMesh;
	private int highScore;
	
	void Start () 
	{
		highScore = PlayerPrefs.GetInt("HighScore");
		textMesh.text = highScore.ToString();
	}

	public void checkScore(int newScore)
	{
		if(newScore > highScore)
		{
			highScore = newScore;
			textMesh.text = highScore.ToString();
		}
	}
	
	void OnDestroy()
	{
		PlayerPrefs.SetInt("HighScore", highScore);
	}
}
